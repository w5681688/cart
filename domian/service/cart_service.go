package service2

import (
	"gitee.com/w5681688/cart/domian/model"
	"gitee.com/w5681688/cart/domian/repository"
)

type ICartService interface {
	AddCart(*model.Cart) (i int64, int64 error)
	DeleteCart(int64) error
	UpdateCart(*model.Cart) error
	FindCartByID(int64) (*model.Cart, error)
	FindAllCart(int64) ([]model.Cart, error)

	ClearCart(int64) error
	IncrNum(int64, int64) error
	DecrNum(int64, int64) error
}

type CartService struct {
	CartRepository repository.ICartRepository
}

func NewCartService(cartRepository repository.ICartRepository) ICartService {
	return &CartService{cartRepository}
}

func (c *CartService) AddCart(cart *model.Cart) (cartId int64, err error) {
	return c.CartRepository.CreateCart(cart)
}

func (c *CartService) DeleteCart(cartId int64) error {
	return c.CartRepository.DeleteCartById(cartId)
}

func (c *CartService) UpdateCart(cart *model.Cart) error {
	return c.CartRepository.UpdateCart(cart)
}

func (c *CartService) FindCartByID(cartId int64) (*model.Cart, error) {
	return c.CartRepository.FindCartById(cartId)
}

func (c *CartService) FindAllCart(userId int64) ([]model.Cart, error) {
	return c.CartRepository.FindAll(userId)
}

func (c *CartService) ClearCart(userId int64) error {
	return c.CartRepository.ClearCart(userId)
}

func (c *CartService) IncrNum(cartId int64, num int64) error {
	return c.CartRepository.IncrNum(cartId, num)
}

func (c *CartService) DecrNum(cartId int64, num int64) error {
	return c.CartRepository.DecrNum(cartId, num)
}
